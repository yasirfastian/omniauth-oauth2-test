require 'omniauth-oauth2'
require 'securerandom'
require 'multi_json'
require 'uri'


module OmniAuth
  module Strategies
    class Redmine < OmniAuth::Strategies::OAuth2
      option :name, 'redmine'
      option :token_options, []

      # option :nonce, SecureRandom.hex()
      # @nonce = SecureRandom.hex()
      # option :token_params, {headers: {'Authorization' => "Basic " + Base64.strict_encode64("local.main.redmine:http://r.main.com:3000/auth/redmine/callback") }}
      # option :client_options, {


      #   site: 'https://id-qa.dynabic.com',
      #   authorize_url: '/connect/authorize?nonce=#{@nonce}',
      #   token_url: '/connect/token'

        # site: 'https://id-qa.dynabic.com',
        # authorize_url: "/connect/authorize?nonce=#{@nonce}",
        # token_url: '/connect/token'

        # request_url: request_url,
        # token_url: token_url,
        # token_method: :post,
        # header: { Accept: accept_header }
        # auth_scheme: :request_body,

      # }

        # option :headers, { Accept: accept_header }
        # option :provider_ignores_state, true


      uid {
        puts "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA #{raw_info.inspect}"
        raw_info['email']
      }

      info do
        {
          # puts "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB #{raw_info.inspect}"
          email: raw_info['email'],
        }
      end

      extra do
        { raw_info: raw_info }
      end

      def raw_info
        puts "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD #{raw_info.inspect}"
        # access_token.options[:mode] = :query
        # access_token.options[:param_name] = :access_token
        # https://id-qa.dynabic.com/connect/userinfo?access_token=:token
        # @raw_info ||= access_token.get("https://id-qa.dynabic.com/connect/userinfo.json?oauth_token=#{access_token.token}").parsed
        @raw_info ||= access_token.get('https://id-qa.dynabic.com/connect/userinfo?access_token=:token', {:parse => :json}).parsed
      end

      # def authorize_params
      #   super.merge(response_mode: "form_post", response_type: "id_token token")
      # end

    end
  end
end

# require 'omniauth-oauth2'
# require 'securerandom'

# module OmniAuth
#   module Strategies
#     class Redmine < OmniAuth::Strategies::OAuth2
#       @nonce = SecureRandom.hex(16)
#       option :name, 'redmine'
#       option :token_params, {headers: {'Authorization' => "Basic " + Base64.strict_encode64("local.main.redmine:http://r.main.com:3000/auth/redmine/callback") }}
#       option :client_options, {
#         # site:          'http://localhost:3000',
#         # authorize_url: 'http://localhost:3000/oauth/authorize',
        
#         # https://idsrv-qa.asposeptyltd.com/identity #https://id-qa.dynabic.com/login

#         site: 'https://id-qa.dynabic.com',
#         authorize_url: "/connect/authorize?nonce=#{@nonce}",
#         token_url: '/connect/token'

#         # redirect_uri: 'http://r.main.com:3000/auth/redmine/callback'


#         # authorize_url: "https://id-qa.dynabic.com/connect/authorize?nonce=#{@nonce}&scope=openid+profile+roles&response_type=id_token+token",
#         # redirect_uri: 'http://r.main.com:3000/login'
        

#         # callback_url: 'http://r.main.com:3000/login'

#       }

#       option :provider_ignores_state, true


#       uid {
#         raw_info['email']
#       }

#       info do
#         {
#           # :name => raw_info['name'],
#           :email => raw_info['email']
#         }
#       end

#       extra do
#         { raw_info: raw_info }
#       end

#       def raw_info
#         # puts "=================================ttttttttttttttttt: #{access_token.inspect}"
#         # @raw_info ||= access_token.get('https://id.dynabic.com/connect/userinfo?access_token=:token').parsed
#         # @raw_info ||= JSON.load(access_token.get('/userinfo.json')).body
#         # @raw_info ||= JSON.load(access_token.get('/me.json')).body
#         # access_token.options[:mode] = :query
#         # access_token.options[:param_name] = :access_token        
#         @raw_info ||= access_token.get('/connect/userinfo', {:parse => :json}).parsed

#       end

#       def authorize_params
#         super.merge(response_mode: "form_post", response_type: "id_token token")
#       end

#     end
#   end
# end