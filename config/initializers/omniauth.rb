require 'redmine'
require 'securerandom'

# OmniAuth.config.logger = Rails.logger


Rails.application.config.middleware.use OmniAuth::Builder do

 	# provider :redmine, 'local.main.redmine', 'http://r.main.com:3000/auth/redmine/callback'
  provider :redmine, #'local.main.redmine', 'http://r.main.com:3000/auth/redmine/callback',
  # provider_ignores_state: true, 
  # scope: 'openid profile email',
  # callback_path: "/auth/redmine/callback",
  # :token_params => { parse: :json },
  # :include_granted_scopes => 'true',
   # {"Content-Type","application/x-www-form-urlencoded"}

  setup: lambda { |env|

  	opts = env['omniauth.strategy'].options     


    # puts "iiiiiiiiiiiiiiiiiiiinnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn:::::::::::::::::::::::::::::::::: #{opts.inspect}"
                   
  	opts[:client_id] = 'teams-qa.dynabic.com'                        
  	opts[:client_secret] = 'https://teams-qa.dynabic.com/auth/redmine/callback'

  	opts[:provider_ignores_state] = true
    opts[:scope] = 'openid profile email'
    opts[:nonce] = SecureRandom.hex(24)
    opts[:code] = SecureRandom.hex(24)

  	opts[:authorize_params][:response_type] = 'id_token token'                       
  	opts[:authorize_params][:response_mode] = 'form_post'
  	opts[:auth_token_params] = { :redirect_uri => "https://teams-qa.dynabic.com/auth/redmine/callback",access_token: :token, parse: :json }                       
  	
    opts[:token_params] = { parse: :json }
    # opts[:token_params] = { :redirect_uri => "http://r.main.com/auth/redmine/callback" }
  	# opts[:token_params] = {headers: {'Authorization' => "Basic " + Base64.strict_encode64("local.main.redmine:http://r.main.com:3000/auth/redmine/callback") }}
  	
    opts[:client_options] = {
  		site: 'https://id-qa.dynabic.com',
      authorize_url: "https://id-qa.dynabic.com/connect/authorize?nonce=#{opts[:nonce]}",
      token_url: 'https://id-qa.dynabic.com/connect/token?action=openidconncallback&access_token=:token'
      # auth_scheme: :request_body,
      # token_method: :query
    }

  # raise Error.new(response) if options[:raise_errors] && !(response.parsed.is_a?(Hash) && response.parsed['access_token'])
  #   AccessToken.from_hash(self, response.parsed.merge(access_token_opts))
  # end


  puts "-----------------------------------------------------------: #{env['omniauth.strategy'].options.inspect}" 
   
  # puts response.parsed['access_token'].inspect
  # puts AccessToken.from_hash(self, response.parsed.merge(access_token_opts)).inspect

  }


  # {
  #   :scope => 'openid profile email',
  #   :provider_ignores_state => true,
  #   :token_params => { :parse => 'json' },
  #   :prompt => 'select_account'

  # }


end