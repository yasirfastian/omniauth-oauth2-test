Rails.application.routes.draw do
  get 'home/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: redirect('/auth/redmine')

  # get '/auth/:provider/callback' => 'application#authentication_callback' 

  get '/auth/:provider/callback' => 'home#index' , :via => [:get, :post]

end
